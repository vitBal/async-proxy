package com.assignment.test.exception;

import lombok.Getter;

public class CVNotFoundException extends BaseException{
    @Getter
    private final String errorCode="NO_SUCH_CV";
    @Getter
    private final String errorDesc="Could not find CV by given id";
    public CVNotFoundException(String message) {
        super(message);
    }
}
