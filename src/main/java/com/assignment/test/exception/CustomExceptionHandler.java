package com.assignment.test.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice
public class CustomExceptionHandler {

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(Throwable.class)
    public ErrorMessage handleAllUnhandledExceptions(Throwable e) {
        log.error("Unhandled exception", e);
        return new ErrorMessage("Internal server error", "UNHANDLED_EXCEPTION");
    }

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(BaseException.class)
    public ErrorMessage handleKnownExceptions(BaseException e) {
        log.error("Processed error during execution", e);
        return new ErrorMessage(e);
    }
}
