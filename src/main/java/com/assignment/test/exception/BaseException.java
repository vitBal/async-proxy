package com.assignment.test.exception;

import lombok.Getter;

public class BaseException extends RuntimeException{
    @Getter
    private final String errorCode = "BASE_ERROR";
    @Getter
    private final String errorDesc = "BASE_DESC";

    public BaseException(String message) {
        super(message);
    }
}
