package com.assignment.test.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ErrorMessage {

    private String cause;
    private String errorCode;

    public ErrorMessage(BaseException e) {
        this.cause = e.getErrorCode();
        this.errorCode = e.getErrorCode();
    }
}
