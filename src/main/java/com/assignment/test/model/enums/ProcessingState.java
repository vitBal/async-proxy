package com.assignment.test.model.enums;

public enum ProcessingState {
    NEW, IN_PROGRESS, COMPLETED, FAILED
}
