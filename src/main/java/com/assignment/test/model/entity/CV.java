package com.assignment.test.model.entity;

import com.assignment.test.model.enums.ProcessingState;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CV {
    private Long id;
    private MultipartFile rawCV;
    private ProcessingState state;
    private String processedCV;
    private Instant createdTime;
    private String processingError;
}
