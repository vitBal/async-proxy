package com.assignment.test.web;

import com.assignment.test.web.dto.CVDto;
import com.assignment.test.web.handler.IntegrationHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequiredArgsConstructor
@RequestMapping()
public class CVProcessingController {

    private final IntegrationHandler integrationHandler;

    public interface Path {
        String BASE = "/test/assignment";
        String SUBMIT = BASE + "/submit";
        String RETRIEVE = BASE + "/retrieve/{processId}";
    }

    @PostMapping(Path.SUBMIT)
    public Long extractCVAndGetProcessId(@RequestParam("file") @Valid @NotNull MultipartFile file){
        return integrationHandler.asyncProcessCV(file);
    }

    @GetMapping(Path.RETRIEVE)
    public CVDto getById(@PathVariable Long processId){
        return integrationHandler.getById(processId);
    }
}
