package com.assignment.test.web.dto;

import com.assignment.test.model.entity.CV;
import com.assignment.test.model.enums.ProcessingState;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CVDto {
    private Long id;
    private ProcessingState state;
    private String processedCV;

    public static CVDto fromEntity(CV cv){
        return new CVDto(cv.getId(), cv.getState(), cv.getProcessedCV());
    }
}
