package com.assignment.test.web.handler;

import com.assignment.test.web.dto.CVDto;
import org.springframework.web.multipart.MultipartFile;

public interface IntegrationHandler {
    Long asyncProcessCV(MultipartFile fileCV);
    CVDto getById(Long id);
}
