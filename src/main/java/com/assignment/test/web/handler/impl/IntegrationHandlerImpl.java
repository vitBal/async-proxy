package com.assignment.test.web.handler.impl;

import com.assignment.test.business.IntegrationService;
import com.assignment.test.business.StorageService;
import com.assignment.test.web.dto.CVDto;
import com.assignment.test.web.handler.IntegrationHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Component
@RequiredArgsConstructor
public class IntegrationHandlerImpl implements IntegrationHandler {

    private final IntegrationService integrationService;
    private final StorageService storageService;

    @Override
    public Long asyncProcessCV(@RequestParam("file") @Valid @NotNull MultipartFile fileCV) {
        return integrationService.saveCVAndAddToProcessing(fileCV);
    }

    @Override
    public CVDto getById(@Valid @NotNull Long id) {
        return CVDto.fromEntity(storageService.getById(id));
    }
}
