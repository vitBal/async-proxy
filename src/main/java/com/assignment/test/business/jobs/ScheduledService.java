package com.assignment.test.business.jobs;

import com.assignment.test.business.StorageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class ScheduledService {

    private final StorageService storageService;

    /**
     * worker that cleans up memory from CVs older than 1 day
     */
    @Scheduled(fixedDelay = 30 * 60 * 1000)
    public void stopLabAfterTimeExpired() {
        log.debug("Start cleaning of processed CVs from storage job");
        List<Long> idsToDelete = storageService.getOldCVs();
        storageService.deleteByIds(idsToDelete);
    }
}
