package com.assignment.test.business.impl;

import com.assignment.test.business.ExtractionService;
import com.assignment.test.business.QueueService;
import com.assignment.test.business.StorageService;
import com.assignment.test.exception.BaseException;
import com.assignment.test.model.entity.CV;
import com.assignment.test.model.enums.ProcessingState;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.ArrayBlockingQueue;

@Slf4j
@Component
@RequiredArgsConstructor
public class QueueServiceImpl implements QueueService {

    private final ExtractionService extractionService;
    private final StorageService storageService;

    private final ArrayBlockingQueue<Long> queue = new ArrayBlockingQueue<>(1024);

    @PostConstruct
    public void processingThreadInit() {
        Thread queueProcessingThread = new Thread(() -> {
            while (true) {
                Long cvId = getNextElementToProcess();
                try {
                    extractionService.processCV(cvId);
                } catch (InterruptedException e) {
                    log.error("Interrupted exception occurred while extracting CV: {}", e.getMessage());
                } catch (Exception ex){
                    log.error("Unhandled exception during processing CV: {}", ex.getMessage());
                    CV byId = storageService.getById(cvId);
                    byId.setState(ProcessingState.FAILED);
                    byId.setProcessingError(ex.getMessage());
                    storageService.update(byId);
                }
            }
        });
        queueProcessingThread.start();
    }

    @Override
    public Long getNextElementToProcess() {
        try {
            return queue.take();
        } catch (InterruptedException e) {
            log.error("Interrupted exception occurred while extracting CV: {}", e.getMessage());
            throw new BaseException("Error in processing CVs queue");
        }
    }

    @Override
    public void put(Long id) {
        try {
            queue.put(id);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
