package com.assignment.test.business.impl;

import com.assignment.test.business.IntegrationService;
import com.assignment.test.business.QueueService;
import com.assignment.test.business.StorageService;
import com.assignment.test.model.entity.CV;
import com.assignment.test.model.enums.ProcessingState;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.time.Instant;

@Slf4j
@Component
@RequiredArgsConstructor
public class IntegrationServiceImpl implements IntegrationService{

    private final StorageService storageService;
    private final QueueService queueService;

    public Long saveCVAndAddToProcessing(MultipartFile fileCV) {
        CV cv = new CV(null, fileCV, ProcessingState.NEW, null, Instant.now(), null);
        storageService.save(cv);
        queueService.put(cv.getId());
        return cv.getId();
    }
}

