package com.assignment.test.business.impl;

import com.assignment.test.business.StorageService;
import com.assignment.test.exception.CVNotFoundException;
import com.assignment.test.model.entity.CV;
import com.assignment.test.model.enums.ProcessingState;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
public class StorageServiceServiceImpl implements StorageService {

    private final ConcurrentHashMap<Long, CV> storage = new ConcurrentHashMap<>();


    @Override
    public CV save(CV cv) {
        Long cvId = storage.mappingCount() + 1;
        cv.setId(cvId);
        return storage.put(cvId, cv);
    }

    @Override
    public CV getById(Long id) {
        CV cv = storage.get(id);
        if (cv ==null){
            throw new CVNotFoundException("Requested CV does not exist with id: " + id);
        }
        return cv;
    }

    @Override
    public CV update(CV cv) {
        return storage.put(cv.getId(), cv);
    }

    public List<Long> getOldCVs(){
        return storage.values().stream()
                .filter(cv -> cv.getState() == ProcessingState.COMPLETED && cv.getCreatedTime().isBefore(Instant.now().minusSeconds(TimeUnit.DAYS.toSeconds(1L))))
                .map(CV::getId)
                .collect(Collectors.toList());
    }

    @Override
    public void deleteByIds(List<Long> ids) {
        ids.forEach(storage::remove);
    }
}
