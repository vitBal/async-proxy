package com.assignment.test.business.impl;

import com.assignment.test.business.ExtractionService;
import com.assignment.test.business.StorageService;
import com.assignment.test.model.entity.CV;
import com.assignment.test.model.enums.ProcessingState;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Slf4j
@Component
@RequiredArgsConstructor
public class ExtractionServiceImpl implements ExtractionService {
     private final StorageService storageService;

    @Override
    public void processCV(Long id) throws InterruptedException {
            CV cv = storageService.getById(id);

            cv.setState(ProcessingState.IN_PROGRESS);
            storageService.update(cv);

            Thread.sleep(TimeUnit.SECONDS.toMillis(20));

            cv.setProcessedCV("</xml>");
            cv.setState(ProcessingState.COMPLETED);
            storageService.update(cv);
    }
}
