package com.assignment.test.business;

import org.springframework.web.multipart.MultipartFile;

public interface IntegrationService {
    Long saveCVAndAddToProcessing(MultipartFile fileCV);
}
