package com.assignment.test.business;

import com.assignment.test.model.entity.CV;

import java.util.List;

/**
 * Storage Service that simulates DB
 */
public interface StorageService {

    CV save(CV cv);

    CV getById(Long id);

    CV update(CV cv);

    List<Long> getOldCVs();

    void deleteByIds(List<Long> ids);
}
