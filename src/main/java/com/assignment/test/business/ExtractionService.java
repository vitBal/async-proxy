package com.assignment.test.business;

/**
 * Mocked service that processes CVs
 */
public interface ExtractionService {
    void processCV(Long id) throws InterruptedException;
}
