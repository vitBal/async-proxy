package com.assignment.test.business;

/**
 * Queue Service that simulates a message broker and processes its element
 */
public interface QueueService {
    Long getNextElementToProcess();
    void put (Long id);
}
